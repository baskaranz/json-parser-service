import org.scalatest.AsyncFlatSpec
import org.scalatest.MustMatchers.convertToAnyMustWrapper
import org.scalatest.concurrent.ScalaFutures
import play.api.libs.json.Json
import services.JsonParser

class JsonParserSpec extends AsyncFlatSpec with ScalaFutures {

  val jsonParser: JsonParser = new JsonParser()

  "JsonParser#mapJsonToSnackModel" should
    "return a message when json is inferred and mapped to Candy model" in {
    val candyJson = Json.parse(
      """
        |{
        |    "name": "abc",
        |    "ingredients": [{
        |        "name": "gmo"
        |    }
        |    ],
        |    "price": 1
        |}
        |""".stripMargin)
      jsonParser.mapJsonToSnackModel(candyJson) map { result =>
        result mustBe "Json mapped to model : models.Candy"
      }
    }

  it should
    "return a message when json is inferred and mapped to Chocolate model" in {
    val candyJson = Json.parse(
      """
        |{
        |    "name": "abc",
        |    "origin": "au",
        |    "ingredients": [{
        |        "name": "gmo"
        |    }
        |    ],
        |    "price": 1
        |}
        |""".stripMargin)
    jsonParser.mapJsonToSnackModel(candyJson) map { result =>
      result mustBe "Json mapped to model : models.Chocolate"
    }
  }

  it should
    "return a message when json is inferred and mapped to Fruit model" in {
    val fruitJson = Json.parse(
      """
        |{
        |    "name": "abc",
        |    "ingredients": [{
        |        "name": "gmo"
        |    }
        |    ],
        |    "price": 1,
        |    "isOrganic": true
        |}
        |""".stripMargin)
    jsonParser.mapJsonToSnackModel(fruitJson) map { result =>
      result mustBe "Json mapped to model : models.Fruit"
    }
  }

  it should
    "throw exception" in {
    recoverToSucceededIf[IllegalArgumentException] {
      val json = Json.parse(
        """
          |{
          |    "abc": "xyz"
          |}
          |""".stripMargin)
      jsonParser.mapJsonToSnackModel(json)
    }
  }

}

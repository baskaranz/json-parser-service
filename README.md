# Json Parser Service
## A simple json parsing REST service to parse a given json payload and map to an appropriate model class.

## Problem statement
---
Please write *production grade*code for JSON parsing. The challenge for the parser logic is that it must be able to support interfaces of the data structures it parses. 
For example, assume a class exists for candy: 
case class Candy(price: Double, ingredients: Set[Ingredient]) 
Where the ingredients are defined as an interface: 
trait Ingredient { 
  val name: String
}

case class NaturalIngredient(name: String) extends Ingredient
case class ArtificialIngredient(name: String) extends Ingredient 
The parser should be able to parse the following JSON 
val json = {
  “price” : 3.14,
  “ingredients” : [ 
    … // JSON structures for natural and/or artificial ingredients 
  ]
} 
into an instance of the Candy class (for example using play-json for the parser): 
val candy = Json.fromJson[Candy](Json.parse(json)) 
The parser code should extensible, that is, another type of ingredient should easily be able to be added. Furthermore, the code should be reusable. Meaning that other data types (e.g. Fruit) should also be able to use the same logic for handling interfaces. 

## Implementation
----
### Language/Framework
- Play/Scala

### Docker registry
- Gitlab's built in Container registry

### Solution overview
The service has a high level trait `Snack` which is extended by 
	- `Candy`
	- `Chocolate`
	- `Fruit`
And the `Ingredient`	used by Snack classes are further extended by
	- `Natural Ingredient   `
	- `Artificial Ingredient`

## Usage
---
- Pull the image: `docker pull registry.gitlab.com/baskaranz/json-parser-service`
- Running the service: `docker run -p 9000:9000 -e APPLICATION_SECRET="28366d34eb792c66dd2284892d337263" registry.gitlab.com/baskaranz/json-parser-service`
	- *Use any port you wish to run the container*
	- *Using any other values for APPLICATION_SECRET would fail*

## Solution endpoints
---
- POST /parse
### Example:
- http://localhost:9000/parse
- Valid mappable json
Request:
```
{
    "name": "Cadbury",
    "ingredients": [],
    "origin": "au",
    "price": 9.99
}
```
Response:
```
{
    “message”: “Json mapped to model : models.Chocolate”
}
```
- Invalid (unmappable) json
Request:
```
{
    “abc”: “xyz”
}
```
Response:
```
{
    "message": "Unable to map json to a model",
    "error": "{\"obj.price\":[{\"msg\":[\"error.path.missing\"],\"args\":[]}],\"obj.ingredients\":[{\"msg\":[\"error.path.missing\"],\"args\":[]}]}"
}
```

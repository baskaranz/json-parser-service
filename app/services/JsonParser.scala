package services

import models.Snack
import play.api.Logger
import play.api.libs.json.{JsError, JsSuccess, JsValue}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class JsonParser {

  val logger: Logger = Logger(this.getClass())

  /**
   * Method to map a json to a model
   * @param json The JsValue of the parsed json request
   * @return a message on the outcome of trying to map the model
   */
  def mapJsonToSnackModel(json: JsValue): Future[String] = {
    Future {
      json.validate[Snack] match {
        case JsSuccess(value, _) =>
          val message = generateMessage(value)
          logger.info(message)
          message
        case e: JsError =>
          logger.error(JsError.toJson(e).toString())
          throw new IllegalArgumentException(JsError.toJson(e).toString())
      }
    }
  }

  private def generateMessage[T](value: T): String = {
    val message = value.getClass.getCanonicalName
    s"Json mapped to model : ${message}"
  }
}

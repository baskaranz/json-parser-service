package models

import play.api.libs.json.{Format, JsResult, JsValue, Json}

trait Snack {
  val name: String
  val ingredients: Seq[Ingredient]
}

case class Candy(name: String, ingredients: Seq[Ingredient], price: Double) extends Snack

case class Chocolate(name: String, ingredients: Seq[Ingredient], origin: String, price: Double) extends Snack

case class Fruit(name: String, ingredients: Seq[Ingredient], isOrganic: Boolean, price: Double) extends Snack

object Candy {
  implicit lazy val candyFormat = Json.format[Candy]
}

object Chocolate {
  implicit lazy val chocolateFormat = Json.format[Chocolate]
}

object Fruit {
  implicit lazy val fruitFormat = Json.format[Fruit]
}

object Snack {
  implicit lazy val snackFormat = new Format[Snack] {
    override def writes(o: Snack): JsValue = o match {
      case chocolate: Chocolate => Json.toJson(chocolate)
      case candy: Candy => Json.toJson(candy)
      case fruit: Fruit => Json.toJson(fruit)
    }

    override def reads(json: JsValue): JsResult[Snack] =
      Chocolate.chocolateFormat.reads(json).orElse(
        Fruit.fruitFormat.reads(json).orElse(
          Candy.candyFormat.reads(json)))
  }
}
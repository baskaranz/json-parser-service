package models

import play.api.libs.json.{Format, JsResult, JsValue, Json}

trait Ingredient {
  val name: String
}

case class ArtificialIngredient(name: String) extends Ingredient

case class NaturalIngredient(name: String) extends Ingredient

object ArtificialIngredient {
  implicit lazy val artificialIngredientFormat = Json.format[ArtificialIngredient]
}

object NaturalIngredient {
  implicit lazy val naturalIngredientFormat = Json.format[NaturalIngredient]
}

object Ingredient {
  implicit lazy val ingredientFormat = new Format[Ingredient] {
    override def writes(o: Ingredient): JsValue = o match {
      case a: ArtificialIngredient => Json.toJson(a)
      case a: NaturalIngredient => Json.toJson(a)
    }
    override def reads(json: JsValue): JsResult[Ingredient] =
      ArtificialIngredient.artificialIngredientFormat.reads(json).orElse(
        NaturalIngredient.naturalIngredientFormat.reads(json))
  }
}
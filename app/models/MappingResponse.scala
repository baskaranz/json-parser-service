package models

import play.api.libs.json.Json

sealed trait MappingResponse {
  val message: String
}

case class MappingSuccess(message: String) extends MappingResponse

case class MappingFailure(message: String) extends MappingResponse

case class MappingFailureWithError(message: String, error: String) extends MappingResponse

object MappingSuccess {
  implicit lazy val successFormat = Json.format[MappingSuccess]
}

object MappingFailure {
  implicit lazy val failureFormat = Json.format[MappingFailure]
}

object MappingFailureWithError {
  implicit lazy val failureWithErrorFormat = Json.format[MappingFailureWithError]
}
package controllers

import models.{MappingFailureWithError, MappingSuccess, Snack}
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import services.JsonParser

import javax.inject._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class JsonParserController @Inject()(cc: ControllerComponents, jsonParser: JsonParser) extends AbstractController(cc) {

  /**
   * Controller to parse/validate json and return if the json
   * maps to any available model
   */
  def parser: Action[JsValue] = Action.async(parse.json) { implicit request =>
    jsonParser.mapJsonToSnackModel(request.body) map { f =>
      Ok(Json.toJson(MappingSuccess(f)))
    } recover {
      case t: Throwable =>
        BadRequest(Json.toJson(MappingFailureWithError("Unable to map json to a model", t.getMessage)))
    }
  }

}

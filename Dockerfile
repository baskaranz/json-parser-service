# Pull sbt image
FROM mozilla/sbt
WORKDIR /build
ADD . .
RUN sbt dist

ENV APPLICATION_SECRET 28366d34eb792c66dd2284892d337263

# Pull java image
FROM openjdk:10.0.1-slim
WORKDIR /app
COPY --from=0 /build/target/universal/*.zip /app/docker build -t nginx-image .
RUN unzip /app/*.zip -d /app/
RUN mv /app/json-parser-service-*/* /app/

EXPOSE 9000

# run the server when a container based on this image is being run
ENTRYPOINT [ "/app/bin/json-parser-service" ]

# EOF
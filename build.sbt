name := "json-parser-service"

maintainer := "baskaranz@gmail.com"

version := "1.0"

lazy val `json-parser-service` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
  jdbc,
  ehcache,
  ws,
  specs2 % Test,
  guice,
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.3" % "test"
)

unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
      